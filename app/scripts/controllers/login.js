'use strict';

/**
 * @ngdoc function
 * @name helloApp.controller:AboutCtrl
 * @description
 * # LoginCtrl
 * Controller of the helloApp
 */
angular.module('helloApp')
  .controller('LoginCtrl', ['$scope',function ($scope) {
    $scope.user = { name: 'VentureOak', password: '1234'}
    $scope.loading = false;
    $scope.submit = function () {
      console.error('No handling provided for this...');
    };
  }]);
